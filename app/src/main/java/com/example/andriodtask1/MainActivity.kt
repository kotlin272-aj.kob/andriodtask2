package com.example.andriodtask1

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : AppCompatActivity() {
    private val REQUEST_SUM = 11
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnSumMenu.setOnClickListener{
            val intent = Intent(MainActivity@this,MathSumActivity::class.java)
            intent.putExtra("correct", txtAmountCorrectMain.text);
            intent.putExtra("wrong", txtAmountWrongMain.text);
            startActivityForResult(intent,REQUEST_SUM)
        }
        btnSubMenu.setOnClickListener{
            val intent = Intent(MainActivity@this,MathSubActivity::class.java)
            intent.putExtra("correct", txtAmountCorrectMain.text);
            intent.putExtra("wrong", txtAmountWrongMain.text);
            startActivityForResult(intent,REQUEST_SUM)
        }
        btnDevideMenu.setOnClickListener{
            val intent = Intent(MainActivity@this,MathDevideActivity::class.java)
            intent.putExtra("correct", txtAmountCorrectMain.text);
            intent.putExtra("wrong", txtAmountWrongMain.text);
            startActivityForResult(intent,REQUEST_SUM)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_SUM) {
            if (resultCode == Activity.RESULT_OK) {
                txtAmountCorrectMain.text =  data?.getStringExtra("correct")
                txtAmountWrongMain.text =  data?.getStringExtra("wrong")
            } else if (resultCode == Activity.RESULT_CANCELED) {
                //TODO Handle Result Cancel
            }
        }
    }
}